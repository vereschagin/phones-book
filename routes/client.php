<?php
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\CatsController;
use App\Http\Controllers\PhonesController;
use App\Http\Controllers\AuthController;

// App
Route::group([
    'domain' => config('app.domain_app'),
    'middleware' => ['web'],
], function () {
    // Index
    Route::view('/', 'client/index');

    // Auth
    Route::post('/api/psch', [AuthController::class, 'checkPassw']);

    // Api
    // Получение списка категорий
    Route::get('/api/cats', [CatsController::class, 'getList']);


    // Создание
    Route::post('/api/cats', [CatsController::class, 'create']);
    // Чтение
    Route::get('/api/cats/{id}', [CatsController::class, 'get']);
    // Редактирование
    Route::put('/api/cats/{id}', [CatsController::class, 'update']);
    // Удаление
    Route::delete('/api/cats/{id}', [CatsController::class, 'delete']);


    // Создание
    Route::post('/api/phones', [PhonesController::class, 'create']);
    // Чтение
    Route::get('/api/phones/{id}', [PhonesController::class, 'get']);
    // Редактирование
    Route::put('/api/phones/{id}', [PhonesController::class, 'update']);
    // Удаление
    Route::delete('/api/phones/{id}', [PhonesController::class, 'delete']);


    // App
    Route::get('{any}', function () {
        return view('client/index');
    })->where('any', '.*');

});

