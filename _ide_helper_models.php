<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Context\Cats\Models{
/**
 * App\Context\Nimbles\Models\NimblesModel
 *
 * @property int $id
 * @property string $nimble_id
 * @property string $type
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|CatsModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CatsModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CatsModel query()
 * @method static \Illuminate\Database\Eloquent\Builder|CatsModel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CatsModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CatsModel whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CatsModel whereNimbleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CatsModel whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CatsModel whereUpdatedAt($value)
 */
	class NimblesModel extends \Eloquent {}
}

namespace App\Context\Projects{
/**
 * App\Context\Projects\ProjectsModel
 *
 * @property int $id
 * @property int $user_id
 * @property string $title
 * @property string $api_token
 * @property string|null $ip_list
 * @property int|null $default_output_width
 * @property int|null $default_output_height
 * @property int|null $default_output_bitrate
 * @property int|null $default_output_framerate
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Context\Streams\Models\StreamModel[] $streams
 * @property-read int|null $streams_count
 * @method static \Illuminate\Database\Eloquent\Builder|ProjectsModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProjectsModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProjectsModel query()
 * @method static \Illuminate\Database\Eloquent\Builder|ProjectsModel whereApiToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProjectsModel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProjectsModel whereDefaultOutputBitrate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProjectsModel whereDefaultOutputFramerate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProjectsModel whereDefaultOutputHeight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProjectsModel whereDefaultOutputWidth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProjectsModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProjectsModel whereIpList($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProjectsModel whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProjectsModel whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProjectsModel whereUserId($value)
 */
	class ProjectsModel extends \Eloquent implements \Illuminate\Contracts\Auth\Authenticatable {}
}

namespace App\Context\Rivers\Models{
/**
 * App\Context\Rivers\Models\RiversModel
 *
 * @property int $id
 * @property int $group_id
 * @property string $river_id
 * @property string $hostname
 * @property string $ip
 * @property int $api_port
 * @property string $cpu_usage
 * @property string $ram_usage
 * @property string $disk_free_gb
 * @property int $machine_uptime
 * @property string $max_decode
 * @property string $used_decode
 * @property string $max_encode
 * @property string $used_encode
 * @property \Illuminate\Database\Eloquent\Collection|\App\Context\Streams\Models\StreamModel[] $streams
 * @property string $json_status
 * @property string|null $last_status
 * @property string $role
 * @property int $is_active
 * @property-read int|null $streams_count
 * @method static \Illuminate\Database\Eloquent\Builder|RiversModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RiversModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RiversModel query()
 * @method static \Illuminate\Database\Eloquent\Builder|RiversModel whereApiPort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RiversModel whereCpuUsage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RiversModel whereDiskFreeGb($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RiversModel whereGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RiversModel whereHostname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RiversModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RiversModel whereIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RiversModel whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RiversModel whereJsonStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RiversModel whereLastStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RiversModel whereMachineUptime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RiversModel whereMaxDecode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RiversModel whereMaxEncode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RiversModel whereRamUsage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RiversModel whereRiverId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RiversModel whereRole($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RiversModel whereStreams($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RiversModel whereUsedDecode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RiversModel whereUsedEncode($value)
 */
	class RiversModel extends \Eloquent {}
}

namespace App\Context\Stat\Models{
/**
 * App\Context\Stat\Models\StatPipelineModel
 *
 * @property int $id
 * @property int|null $pipeline_id
 * @property int $stat_stream_id
 * @property string $name
 * @property int $width
 * @property int $height
 * @property int $bitrate
 * @property string $dest
 * @property int $data_flow
 * @property string $start_time
 * @property string $end_time
 * @property int $finished
 * @property string $amount_charged
 * @property string $amount_to_charge
 * @property string $amount_charging
 * @property string|null $charging_time
 * @method static \Illuminate\Database\Eloquent\Builder|StatPipelineModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|StatPipelineModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|StatPipelineModel query()
 * @method static \Illuminate\Database\Eloquent\Builder|StatPipelineModel whereAmountCharged($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatPipelineModel whereAmountCharging($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatPipelineModel whereAmountToCharge($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatPipelineModel whereBitrate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatPipelineModel whereChargingTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatPipelineModel whereDataFlow($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatPipelineModel whereDest($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatPipelineModel whereEndTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatPipelineModel whereFinished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatPipelineModel whereHeight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatPipelineModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatPipelineModel whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatPipelineModel wherePipelineId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatPipelineModel whereStartTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatPipelineModel whereStatStreamId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatPipelineModel whereWidth($value)
 */
	class StatPipelineModel extends \Eloquent {}
}

namespace App\Context\Stat\Models{
/**
 * App\Context\Stat\Models\StatStreamModel
 *
 * @property int $id
 * @property int|null $stream_id
 * @property int|null $river_id
 * @property int $user_id
 * @property string $name
 * @property string $source
 * @property string $river_codename
 * @property string $start_time
 * @property string $end_time
 * @property int $finished
 * @property string|null $video_codec
 * @property int|null $width
 * @property int|null $height
 * @property int|null $framerate
 * @property string|null $audio_codec
 * @property int|null $channels
 * @property int|null $rate
 * @property string $amount_charged
 * @property string $amount_to_charge
 * @property string $amount_charging
 * @property string|null $charging_time
 * @method static \Illuminate\Database\Eloquent\Builder|StatStreamModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|StatStreamModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|StatStreamModel query()
 * @method static \Illuminate\Database\Eloquent\Builder|StatStreamModel whereAmountCharged($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatStreamModel whereAmountCharging($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatStreamModel whereAmountToCharge($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatStreamModel whereAudioCodec($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatStreamModel whereChannels($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatStreamModel whereChargingTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatStreamModel whereEndTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatStreamModel whereFinished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatStreamModel whereFramerate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatStreamModel whereHeight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatStreamModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatStreamModel whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatStreamModel whereRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatStreamModel whereRiverCodename($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatStreamModel whereRiverId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatStreamModel whereSource($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatStreamModel whereStartTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatStreamModel whereStreamId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatStreamModel whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatStreamModel whereVideoCodec($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatStreamModel whereWidth($value)
 */
	class StatStreamModel extends \Eloquent {}
}

namespace App\Context\Streams\Models{
/**
 * App\Context\Streams\Models\PipelineModel
 *
 * @property int $id
 * @property int $stream_id
 * @property string $name
 * @property string|null $playback_id
 * @property int $width
 * @property int $height
 * @property int $bitrate
 * @property int|null $framerate
 * @property string $encode_weight
 * @property string $dest
 * @property int $is_active
 * @property string $state
 * @method static \Illuminate\Database\Eloquent\Builder|PipelineModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PipelineModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PipelineModel query()
 * @method static \Illuminate\Database\Eloquent\Builder|PipelineModel whereBitrate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PipelineModel whereDest($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PipelineModel whereEncodeWeight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PipelineModel whereFramerate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PipelineModel whereHeight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PipelineModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PipelineModel whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PipelineModel whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PipelineModel wherePlaybackId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PipelineModel whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PipelineModel whereStreamId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PipelineModel whereWidth($value)
 */
	class PipelineModel extends \Eloquent {}
}

namespace App\Context\Streams\Models{
/**
 * Class StreamModel
 *
 * @package App\Context\Streams\Models
 * @see \App\Models\StreamModel
 * @property RiversModel $river
 * @property int $id
 * @property int|null $user_id
 * @property int $project_id
 * @property string $type
 * @property string|null $stream_key
 * @property string $name
 * @property string $source
 * @property string|null $nimble_stream_id
 * @property int|null $river_id
 * @property int $auto_distribution
 * @property int $is_active
 * @property string $state
 * @property string|null $video_codec
 * @property int|null $width
 * @property int|null $height
 * @property int|null $framerate
 * @property string $decode_weight
 * @property string|null $audio_codec
 * @property int|null $channels
 * @property int|null $rate
 * @property int|null $custom_id
 * @property string|null $custom_data
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Context\Streams\Models\PipelineModel[] $pipelines
 * @property-read int|null $pipelines_count
 * @property-read \App\Context\Projects\ProjectsModel $project
 * @method static \Illuminate\Database\Eloquent\Builder|StreamModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|StreamModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|StreamModel query()
 * @method static \Illuminate\Database\Eloquent\Builder|StreamModel whereAudioCodec($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StreamModel whereAutoDistribution($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StreamModel whereChannels($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StreamModel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StreamModel whereCustomData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StreamModel whereCustomId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StreamModel whereDecodeWeight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StreamModel whereFramerate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StreamModel whereHeight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StreamModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StreamModel whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StreamModel whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StreamModel whereNimbleStreamId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StreamModel whereProjectId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StreamModel whereRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StreamModel whereRiverId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StreamModel whereSource($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StreamModel whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StreamModel whereStreamKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StreamModel whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StreamModel whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StreamModel whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StreamModel whereVideoCodec($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StreamModel whereWidth($value)
 */
	class StreamModel extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\ConfigModel
 *
 * @property int $id
 * @property string $type
 * @property string $group
 * @property string $name
 * @property string $num_val
 * @property string $str_val
 * @property string $desc
 * @method static \Illuminate\Database\Eloquent\Builder|ConfigModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ConfigModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ConfigModel query()
 * @method static \Illuminate\Database\Eloquent\Builder|ConfigModel whereDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ConfigModel whereGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ConfigModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ConfigModel whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ConfigModel whereNumVal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ConfigModel whereStrVal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ConfigModel whereType($value)
 */
	class ConfigModel extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\PipelineModel
 *
 * @OA\Schema (
 *      schema="Pipeline",
 *      @OA\Xml (name="Pipeline"),
 *      required={"name", "destination"},
 *      @OA\Property (property="id", type="integer", readOnly=true, example="18576", description="Automatic unique outgoing stream (pipeline) ID"),
 *      @OA\Property (property="name", type="string", readOnly=false, example="1280x720_2050kbps_59fc", maxLength=255,
 *          description="Alpha-dashed (URL-friendly) outgoing stream name"),
 *      @OA\Property (property="is_active", type="integer", readOnly=false, example="1",
 *          description="Shows if the outgoing stream (pipeline) is on or off. Can be only 1 or 0"),
 *      @OA\Property (property="state", type="string", readOnly=true, example="playing",
 *          description="Current stream state, like: initial, playing"),
 *      @OA\Property (property="output", type="object", readOnly=false, required={"url"},
 *          @OA\Property (property="url", type="string", readOnly=false, maxLength=2048, example="rtmp:\/\/example.com:555\/test\/720p",
 *              description="Destination URL of the resulting RTMP-stream")
 *      ),
 *      @OA\Property (property="video", type="object", readOnly=false, required={"width", "height", "bitrate"},
 *          @OA\Property (property="width", type="integer", readOnly=false, nullable="true", example="1280",
 *              description="Desired width of the processed video"),
 *          @OA\Property (property="height", type="integer", readOnly=false, nullable="true", example="720",
 *              description="Desired height of the processed video"),
 *          @OA\Property (property="bitrate", type="integer", readOnly=false, nullable="true", example="2000000",
 *              description="Desired bitrate of the processed video. Bits per second"),
 *      ),
 * )
 *
 * Class PipelineModel
 * @package App\Models
 * @deprecated moved to context:
 * @see \App\Context\Streams\Models\PipelineModel
 * @property-read \App\Models\StreamModel $stream
 * @method static \Illuminate\Database\Eloquent\Builder|PipelineModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PipelineModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PipelineModel query()
 */
	class PipelineModel extends \Eloquent {}
}

namespace App\Models{
/**
 * Class RiverModel
 *
 * @package App\Models
 * @deprecated moved to App\Context\Rivers\Models\RiversModel
 * @see RiversModel
 * @property int $id
 * @property int $group_id
 * @property string $river_id
 * @property string $hostname
 * @property string $ip
 * @property int $api_port
 * @property string $cpu_usage
 * @property string $ram_usage
 * @property string $disk_free_gb
 * @property int $machine_uptime
 * @property string $max_decode
 * @property string $used_decode
 * @property string $max_encode
 * @property string $used_encode
 * @property \Illuminate\Database\Eloquent\Collection|\App\Models\StreamModel[] $streams
 * @property string $json_status
 * @property string|null $last_status
 * @property string $role
 * @property int $is_active
 * @property-read int|null $streams_count
 * @method static \Illuminate\Database\Eloquent\Builder|RiverModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RiverModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RiverModel query()
 * @method static \Illuminate\Database\Eloquent\Builder|RiverModel whereApiPort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RiverModel whereCpuUsage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RiverModel whereDiskFreeGb($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RiverModel whereGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RiverModel whereHostname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RiverModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RiverModel whereIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RiverModel whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RiverModel whereJsonStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RiverModel whereLastStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RiverModel whereMachineUptime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RiverModel whereMaxDecode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RiverModel whereMaxEncode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RiverModel whereRamUsage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RiverModel whereRiverId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RiverModel whereRole($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RiverModel whereStreams($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RiverModel whereUsedDecode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RiverModel whereUsedEncode($value)
 */
	class RiverModel extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\StatPipelineModel
 *
 * @property int $id
 * @property int|null $pipeline_id
 * @property int $stat_stream_id
 * @property string $name
 * @property int $width
 * @property int $height
 * @property int $bitrate
 * @property string $dest
 * @property int $data_flow
 * @property string $start_time
 * @property string $end_time
 * @property int $finished
 * @property string $amount_charged
 * @property string $amount_to_charge
 * @property string $amount_charging
 * @property string|null $charging_time
 * @property-read \App\Models\PipelineModel|null $pipeline
 * @property-read \App\Models\StatStreamModel $stat_stream
 * @method static \Illuminate\Database\Eloquent\Builder|StatPipelineModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|StatPipelineModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|StatPipelineModel query()
 * @method static \Illuminate\Database\Eloquent\Builder|StatPipelineModel whereAmountCharged($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatPipelineModel whereAmountCharging($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatPipelineModel whereAmountToCharge($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatPipelineModel whereBitrate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatPipelineModel whereChargingTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatPipelineModel whereDataFlow($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatPipelineModel whereDest($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatPipelineModel whereEndTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatPipelineModel whereFinished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatPipelineModel whereHeight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatPipelineModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatPipelineModel whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatPipelineModel wherePipelineId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatPipelineModel whereStartTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatPipelineModel whereStatStreamId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatPipelineModel whereWidth($value)
 */
	class StatPipelineModel extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\StatStreamModel
 *
 * @property int $id
 * @property int|null $stream_id
 * @property int|null $river_id
 * @property int $user_id
 * @property string $name
 * @property string $source
 * @property string $river_codename
 * @property string $start_time
 * @property string $end_time
 * @property int $finished
 * @property string|null $video_codec
 * @property int|null $width
 * @property int|null $height
 * @property int|null $framerate
 * @property string|null $audio_codec
 * @property int|null $channels
 * @property int|null $rate
 * @property string $amount_charged
 * @property string $amount_to_charge
 * @property string $amount_charging
 * @property string|null $charging_time
 * @property-read \App\Models\RiverModel|null $river
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\StatPipelineModel[] $stat_pipelines
 * @property-read int|null $stat_pipelines_count
 * @property-read \App\Models\StreamModel|null $stream
 * @method static \Illuminate\Database\Eloquent\Builder|StatStreamModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|StatStreamModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|StatStreamModel query()
 * @method static \Illuminate\Database\Eloquent\Builder|StatStreamModel whereAmountCharged($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatStreamModel whereAmountCharging($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatStreamModel whereAmountToCharge($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatStreamModel whereAudioCodec($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatStreamModel whereChannels($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatStreamModel whereChargingTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatStreamModel whereEndTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatStreamModel whereFinished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatStreamModel whereFramerate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatStreamModel whereHeight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatStreamModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatStreamModel whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatStreamModel whereRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatStreamModel whereRiverCodename($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatStreamModel whereRiverId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatStreamModel whereSource($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatStreamModel whereStartTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatStreamModel whereStreamId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatStreamModel whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatStreamModel whereVideoCodec($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatStreamModel whereWidth($value)
 */
	class StatStreamModel extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\StreamModel
 *
 * @OA\Schema (
 *      schema="Stream",
 *      @OA\Xml (name="Stream"),
 *      required={"name", "source"},
 *      @OA\Property (property="id", type="integer", readOnly=true, example="18576", description="Automatic unique stream ID"),
 *      @OA\Property (property="custom_id", type="integer", nullable="true", readOnly=false, example="7", description="Unique stream ID specified by user"),
 *      @OA\Property (property="custom_data", type="string", nullable="true", readOnly=false, example="{'value':'Some custom data'}", maxLength=255,
 *          description="Unique stream ID specified by user"),
 *      @OA\Property (property="name", type="string", readOnly=false, example="Main camera", maxLength=255, description="Human-friendly stream name"),
 *      @OA\Property (property="is_active", type="integer", readOnly=false, example="1", description="Shows if the stream is on or off. Can be only 1 or 0"),
 *      @OA\Property (property="state", type="string", readOnly=true, example="playing",
 *          description="Current stream state, like: initial, starting, playing, paused..."),
 *      @OA\Property (property="source", type="object", readOnly=false, required={"url"},
 *          @OA\Property (property="url", type="string", readOnly=false, maxLength=2048, example="rtsp:\/\/admin:admin@test.com:554\/Streaming\/Camera\/101",
 *              description="Source URL of an RTSP-stream")
 *      ),
 *      @OA\Property (property="properties", type="object", readOnly=true,
 *          @OA\Property (property="video_codec", type="string", readOnly=true, nullable="true", example="H264",
 *              description="Incoming stream video codec. NULL when unknown"),
 *          @OA\Property (property="width", type="integer", readOnly=true, nullable="true", example="1920",
 *              description="Incoming stream video width. NULL when unknown"),
 *          @OA\Property (property="height", type="integer", readOnly=true, nullable="true", example="1080",
 *              description="Incoming stream video height. NULL when unknown"),
 *          @OA\Property (property="framerate", type="integer", readOnly=true, nullable="true", example="25",
 *              description="Incoming stream video framerate. NULL when unknown"),
 *          @OA\Property (property="audio_codec", type="integer", readOnly=true, nullable="true", example="PCMU",
 *              description="Incoming stream audio codec. NULL when unknown"),
 *          @OA\Property (property="channels", type="integer", readOnly=true, nullable="true", example="2",
 *              description="The number of audio channels in incoming stream. NULL when unknown"),
 *          @OA\Property (property="rate", type="integer", readOnly=true, nullable="true", example="8000",
 *              description="Rate of the audio, Hz. NULL when unknown")
 *      )
 * )
 *
 *  @OA\Schema (
 *      schema="StreamEx",
 *      @OA\Xml (name="StreamEx"),
 *      allOf={
 *          @OA\Schema(ref="#/components/schemas/Stream"),
 *      },
 *      @OA\Property (property="pipelines", type="array", readOnly=false, description="List of outgoing streams (pipelines) and their properties",
 *          @OA\Items(ref="#components/schemas/Pipeline")
 *      )
 *  )
 *
 * Class StreamModel
 * @package App\Models
 * @deprecated moved to context
 * @see \App\Context\Streams\Models\StreamModel
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PipelineModel[] $pipelines
 * @property-read int|null $pipelines_count
 * @property-read \App\Models\RiverModel $river
 * @method static \Illuminate\Database\Eloquent\Builder|StreamModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|StreamModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|StreamModel query()
 */
	class StreamModel extends \Eloquent {}
}

