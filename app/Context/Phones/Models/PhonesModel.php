<?php
namespace App\Context\Phones\Models;

use Illuminate\Database\Eloquent\Model;

class PhonesModel extends Model
{
    // The table associated with the model
    protected $table = 'phones';

    // Turn `on` automatic 'created_at' and 'updated_at' columns
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cat_id',
        'name',
        'surname',
        'middlename',
        'position',
        'phone',
        'email',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function toLog()
    {
        return [
            'id' => $this->id,
            'cat_id' => $this->cat_id,
        ];
    }
}
