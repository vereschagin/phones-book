<?php
namespace App\Context\Phones;

use App\Classes\ServerResponse;
use App\Context\Phones\Models\PhonesModel;
use Illuminate\Support\Facades\Log;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request as SRequest;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;


class PhonesService
{
    private $validateRules = null;

    function __construct()
    {
        $this->validateRules = [
            'cat_id' => [
                'required',
                'integer'
            ],

            'name' => [
                'required',
                'string',
                'max:255'
            ],

            'surname' => [
                'required',
                'string',
                'max:255'
            ],

            'middlename' => [
                'required',
                'string',
                'max:255'
            ],

            'position' => [
                'required',
                'string',
                'max:255'
            ],

            'phone' => [
                'required',
                'string',
                'max:255'
            ],

            'email' => [
                'required',
                'string',
                'max:255'
            ],

        ];
    }

    /**
     * Подготовка запроса
     * Добавляем сразу фильтрацию по пользователю и проекту
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    private function query()
    {
        return PhonesModel::query();
    }


    public function getList(): array
    {
        $res = $this->query()
            ->get();
        //
        if (!empty($res)) {
            $res = $res->toArray();
            $res = array_column($res, null, 'id');
        }
        //
        return $res;
    }


    /**
     * Получение данных ривера
     * получаем по `id`
     *
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    public function get (int $id)
    {
        $res = $this->query()
            ->where('id', $id)
            ->get()
            ->first();
        //
        if ($res) {
            $res = $res->toArray();
        } else {
            // To Log
            throw new \Exception('Phones: phone not found #' . $id);
        }
        //
        return $res;
    }

    /**
     * Валидация данных для `create` / `update` методов
     *
     * @param Request $request
     * @return void
     * @throws \Illuminate\Validation\ValidationException
     */
    public function validate (Request $request)
    {
        Validator::make($request->all(), $this->validateRules)->validate();
    }

    /**
     * Вариант валидации "без подробностей" для внутренней логики
     *
     * @param array $request
     * @return bool
     * @throws \Illuminate\Validation\ValidationException
     */
    public function isValid (array $request): bool
    {
        try {
            Validator::make($request, $this->validateRules)->validate();
        } catch (ValidationException $e) {
            return false;
        }
        //
        return true;
    }

    /**
     * @param Request $request
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    public function create (Request $request)
    {
        // Validate
        $this->validate($request);
        // Data
        $data = $request->all(array_keys($this->validateRules));
        // Add User id & Create
        $id = PhonesModel::create($data)->id;
        // To Log
        Log::info(
            'Phones: added new phone.',
            [
                //'user_id' => $this->auth_user_id,
                //'project_id' => $this->auth_project_id,
                'ip' => SRequest::ip(),
                'request' => $request->all()
            ]
        );
        // Response
        return[
            'result' => 'ok',
            'id' => $id,
        ];
    }

    /**
     * @param Request $request
     * @param int $id
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update (Request $request, int $id)
    {
        // Select
        $old_data = $this->get($id);
        // Wrong ID
        if (empty($old_data)) {
            // To Log
            throw new \Exception('Phones: try update WRONG phone #' . $id);
        } else {
            // Validate
            $this->validate($request);
            // Data
            $data = $request->all(array_keys($this->validateRules));
            // Add User id & Update
            $res = $this->query()
                ->where('id', $id)
                ->update($data);
            // To Log
            Log::info(
                'Phones: update phone #' . $id,
                [
                    'ip' => SRequest::ip(),
                    'request' => $request->all(),
                    'old_data' => $old_data
                ]
            );
        }
        // Response
        $response = [
            'result' => 'ok',
        ];
        //
        return $response;
    }

    /**
     * @param Request $request
     * @param int $id
     * @return string[]
     * @throws Exception
     */
    public function delete (Request $request, int $id)
    {
        // Select
        $old_data = $this->get($id);
        // Wrong ID
        if (empty($old_data)) {
            // To Log
            throw new \Exception('Phones: try delete WRONG phone #' . $id);
        } else {
            // Delete
            $res = $this->query()
                ->where('id',$id)
                ->first()
                ->delete();
            // To Log
            Log::info(
                'Phones: deleted phone #' . $id,
                [
                    'ip' => SRequest::ip(),
                    'request' => $request->all(),
                    'old_data' => $old_data
                ]
            );
        }
        // Response
        $response = [
            'result' => 'ok',
        ];
        //
        return $response;
    }
}
