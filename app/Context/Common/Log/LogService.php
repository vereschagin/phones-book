<?php
namespace App\Context\Common\Log;


use Exception;

class LogService
{
    const SENSITIVE = "**sensitive**";

    /**
     * @param null $startedAt
     * @param Exception|null $e
     * @param bool $withAuth - в некоторых случаях аутентификацию не следует вызывать, например при логировании ошибок при аутентификации (иначе возникает ошибка рекурсии)
     * @return array[]
     */
    public static function context($startedAt = null, Exception $e = null, $withAuth = true): array
    {
        $request = request();

        $res = [
            'request' => [
                'path' => $request->path(),
                'ip' => $request->ip(),
                'agent' => $request->userAgent(),
                'params' => self::cleanSensitiveDataFromParams($request->toArray()),
            ],
        ];

        if (!empty($e)) {
            $res['exception'] = get_class($e);
            $res['exception_message'] = $e->getMessage();
        }

        if (!empty($startedAt)) {
            $res['spent_sec'] = self::spentSec($startedAt);
        }

        return $res;
    }

    public static function spentString($startedAt): string
    {
        return self::spentSec($startedAt) . ' sec';
    }

    public static function spentSec($startedAt): float
    {
        return round(microtime(true) - $startedAt, 3);
    }

    public static function cleanSensitiveDataFromParams(array $params)
    {
        foreach ($params as $k => $v) {
            if (preg_match("#(pass|token|code)#i", $k)) {
                $params[$k] = self::SENSITIVE;
            }
        }
        return $params;
    }

}
