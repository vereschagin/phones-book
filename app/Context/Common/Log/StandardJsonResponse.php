<?php


namespace App\Context\Services\Users\Responses;


use Exception;
use Psr\Http\Message\ResponseInterface;

class StandardJsonResponse
{
    /**
     * @var ResponseInterface
     */
    private $response;

    /**
     * @var array
     */
    public $attributes;

    public function __construct(ResponseInterface $response)
    {
        $this->response = $response;
        try {
            $this->attributes = json_decode((string)$this->response->getBody(), true, 512, JSON_THROW_ON_ERROR);
        } catch (Exception $e) {
            // Если json не декодится, возможно в ответе просто текст, возвращаем его как сообщение. Пример: "Wrong IP. Service id#5; checked IP: ..." (403)
            $this->attributes = [
                'message' => $response->getBody(),
            ];
        }
    }

    public function statusCode(): int
    {
        return $this->response->getStatusCode();
    }

    public function body(): string
    {
        return (string)$this->response->getBody();
    }

    /**
     * У всех ответов должен быть сопроводительное сообщение (для пользователя системы)
     * @return string|null
     */
    public function message(): ?string
    {
        return $this->attributes['message'] ?? null;
    }

    public function data(): ?array
    {
        return $this->attributes;
    }

    /**
     * При ошибках валидации будет массив ошибок
     * @return array|null
     */
    public function errors(): ?array
    {
        return $this->attributes['errors'] ?? [];
    }

    public function toLog(): array
    {
        return [
            'code' => $this->statusCode(),
            'body' => $this->body(),
        ];
    }
}
