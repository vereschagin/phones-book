<?php
namespace App\Context\Cats\Models;

use Illuminate\Database\Eloquent\Model;

class CatsModel extends Model
{
    // The table associated with the model
    protected $table = 'cats';

    // Turn `on` automatic 'created_at' and 'updated_at' columns
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'pid',
        'name',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function toLog()
    {
        return [
            'id' => $this->id,
            'pid' => $this->pid,
            'name' => $this->name,
        ];
    }
}
