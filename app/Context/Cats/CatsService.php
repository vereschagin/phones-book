<?php
namespace App\Context\Cats;

use App\Classes\ServerResponse;
use App\Context\Cats\Models\CatsModel;
use App\Context\Phones\Models\PhonesModel;
use App\Context\Phones\PhonesService;
use Illuminate\Support\Facades\Log;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request as SRequest;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;


class CatsService
{
    private $validateRules = null;

    function __construct()
    {
        $this->validateRules = [
            'pid' => [
                'required',
                'integer'
            ],

            'name' => [
                'required',
                'string',
                'max:255'
            ],

        ];
    }


    /**
     * Подготовка запроса
     * Добавляем сразу фильтрацию по пользователю и проекту
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    private function query()
    {
        return CatsModel::query();
    }

    /**
     * @param array $list
     * @param int $pid
     * @param array|null $phones
     * @return array
     */
    private function _getListSubs (array $list, int $pid, ?array $phones = []): array
    {
        $res = [];
        array_map(function($v) use(&$res, $list, $pid, $phones) {
            if ($v['pid'] === $pid) {
                $res[$v['id']] = $v + ['subs' => $this->_getListSubs($list, $v['id'], $phones)];
                if (!empty($phones)) {
                    $res[$v['id']]['phones'] = array_filter($phones, function($v2) use($v) {
                        return $v2['cat_id'] === $v['id'];
                    });
                }
            }
        }, $list);
        return $res;
    }

    /**
     * Получение списка риверов
     *
     * @param array $filters
     * @return false[]
     */
    public function getList(?array $filters): array
    {
        $res = [];
        //
        $list = $this->query()->get();
        //
        if (!empty($list)) {
            $list = $list->toArray();

            if (!empty($filters['with_phones'])) {
                $phones =  app(PhonesService::class)->getList([]);
            }

            $res = $this->_getListSubs($list, 0, $phones ?? []);
        }
        //
        return $res;
    }


    /**
     * Получение данных ривера
     * получаем по `id`
     *
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    public function get (int $id)
    {
        $res = $this->query()
            ->where('id', $id)
            ->get()
            ->first();
        if ($res) {
            $res = $res->toArray();
        } else {
            // To Log
            throw new \Exception('Cats: cat not found #' . $id);
        }

        return $res;
    }


    /**
     * Валидация данных для `create` / `update` методов
     *
     * @param Request $request
     * @return void
     * @throws \Illuminate\Validation\ValidationException
     */
    public function validate (Request $request)
    {
        Validator::make($request->all(), $this->validateRules)->validate();
    }

    /**
     * Вариант валидации "без подробностей" для внутренней логики
     *
     * @param array $request
     * @return bool
     * @throws \Illuminate\Validation\ValidationException
     */
    public function isValid (array $request): bool
    {
        try {
            Validator::make($request, $this->validateRules)->validate();
        } catch (ValidationException $e) {
            return false;
        }
        //
        return true;
    }

    /**
     * @param Request $request
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    public function create (Request $request)
    {
        // Validate
        $this->validate($request);
        // Data
        $data = $request->all(array_keys($this->validateRules));
        // Add User id & Create
        $id = CatsModel::create($data)->id;
        // To Log
        Log::info(
            'Cats: added new cat.',
            [
                'ip' => SRequest::ip(),
                'request' => $request->all()
            ]
        );
        // Response
        return[
            'result' => 'ok',
            'id' => $id,
        ];
    }

    /**
     * @param Request $request
     * @param int $id
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update (Request $request, int $id)
    {
        // Select
        $old_data = $this->get($id);
        // Wrong ID
        if (empty($old_data)) {
            // To Log
            throw new \Exception('Cats: try update WRONG cat #' . $id);
        } else {
            // Validate
            $this->validate($request);
            // Data
            $data = $request->all(array_keys($this->validateRules));
            // Add User id & Update
            $res = $this->query()
                ->where('id', $id)
                ->update($data);
            // To Log
            Log::info(
                'Cats: update cat #' . $id,
                [
                    'ip' => SRequest::ip(),
                    'request' => $request->all(),
                    'old_data' => $old_data
                ]
            );
        }
        // Response
        $response = [
            'result' => 'ok',
        ];
        //
        return $response;
    }

    /**
     * @param Request $request
     * @param int $id
     * @return string[]
     * @throws Exception
     */
    public function delete (Request $request, int $id)
    {
        // Select
        $old_data = $this->get($id);
        // Wrong ID
        if (empty($old_data)) {
            // To Log
            throw new \Exception('Cats: try delete WRONG cat #' . $id);
        } else {
            // Delete
            $res = $this->query()
                ->where('id',$id)
                ->first()
                ->delete();
            // To Log
            Log::info(
                'Cats: deleted cat #' . $id,
                [
                    'ip' => SRequest::ip(),
                    'request' => $request->all(),
                    'old_data' => $old_data
                ]
            );
        }
        // Response
        $response = [
            'result' => 'ok',
        ];
        //
        return $response;
    }
}
