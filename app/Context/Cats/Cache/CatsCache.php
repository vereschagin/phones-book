<?php
namespace App\Context\Cats\Cache;

use App\Context\Performance\Cacher\Cacher;
use Illuminate\Support\Facades\Cache;
use Closure;

class CatsCache
{
    // Настройки кеширования (ключи и время жизни)
    private $cfg = [
        'getList' => [
            'key' => '/nimbles',
            'ttl' => 120
        ],
        'get' => [
            'key' => '/nimbles/id/%d',
            'ttl' => 120
        ],
        'getByNimbleId' => [
            'key' => '/nimbles/nimble_id/%s',
            'ttl' => 120
        ],
    ];

    /**
     * Кеширование списка
     *
     * @param Closure $callback
     * @return mixed
     */
    public function rememberList (Closure $callback)
    {
        return Cacher::remember(
            $this->cfg['getList']['key'],
            $this->cfg['getList']['ttl'],
            $callback
        );
    }

    /**
     * Сброс кеша списка
     *
     * @param Closure $callback
     * @return bool
     */
    public function forgetList (): bool
    {
        return Cacher::forget($this->cfg['getList']['key']);
    }


    /**
     * Кеширование запроса по `nimble_id`
     *
     * @param string $nimble_id
     * @param Closure $callback
     * @return mixed
     */
    public function rememberByNimbleId (string $nimble_id, Closure $callback)
    {
        return Cacher::remember(
            sprintf($this->cfg['getByNimbleId']['key'], $nimble_id),
            $this->cfg['getByNimbleId']['ttl'],
            $callback
        );
    }

    /**
     * Сброс кеша по `nimble_id`
     *
     * @param string $nimble_id
     * @return bool
     */
    public function forgetByNimbleId (string $nimble_id): bool
    {
        return Cacher::forget(sprintf($this->cfg['getByNimbleId']['key'], $nimble_id));
    }


    /**
     * Кеширование запроса по `id`
     *
     * @param integer $id
     * @param Closure $callback
     * @return mixed
     */
    public function rememberById (int $id, Closure $callback)
    {
        return Cacher::remember(
            sprintf($this->cfg['get']['key'], $id),
            $this->cfg['get']['ttl'],
            $callback
        );
    }

    /**
     * Сброс кеша по `id`
     *
     * @param integer $id
     * @return bool
     */
    public function forgetById (int $id): bool
    {
        return Cacher::forget(sprintf($this->cfg['get']['key'], $id));
    }

}
