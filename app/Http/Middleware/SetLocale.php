<?php

namespace App\Http\Middleware;

//use App\Providers\RouteServiceProvider;
use Closure;
//use Illuminate\Http\Request;

class SetLocale {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
	public function handle($request, Closure $next) {
		$locale = $request->segment(1, 'en');

		if ($locale == 'ru') app()->setLocale($locale);

		return $next($request);
	}
}