<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

/**
 * Middleware для более четкой работы API - c этим middleware апишки всегда отдают json, даже если API запрашивается напрямую из браузера.
 *
 * Хак позволяющий переключить определенные маршруты в json only режим.
 * Чтобы исключить встроенные в ларавель редиректы на предыдущую страницу (при ошибках валидации $request->validate()),
 * например в API это не нужно и только путает.
 *
 * Подробнее об особенностях редиректов при ошибках валидации в ларавеле:
 * https://laravel.com/docs/8.x/validation#quick-displaying-the-validation-errors
 *
 * Class ForceHeaderAcceptJsonMiddleware
 * @package App\Http\Middleware
 */
class ApiForceHeaderAcceptJsonMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $request->headers->set('Accept', 'application/json');
        return $next($request);
    }
}
