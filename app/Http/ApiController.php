<?php
namespace App\Http;

use App\Context\Common\Log\LogService;
use Exception;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Validation\ValidationException;
USE Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use App\Context\Services\Users\Responses\StandardJsonResponse;

class ApiController extends BaseController
{
    /**
     * Общая обертка для апи контроллеров, в которой стандартно обрабатываются большинство эксепшенов.
     * @param callable $func
     * @param string $method
     * @return JsonResponse
     */
    public function tryCatchApiResponse(callable $func, string $method): JsonResponse
    {
        $startedAt = microtime(1);

        $response = [];
        $response_state = 200;

        // Debug
        if (config('app.debug')) {
            $response[] = \DB::enableQueryLog();
        }

        try {
            $response = $func();
        } catch (ValidationException $e) {
            $errors = $e->validator->errors()->messages();
            Log::error($method, compact('errors') + LogService::context($startedAt, $e));

            $response_state = 400;
            $response = [
                'result' => 'error',
                'message' => $e->getMessage(),
                'errors' => $errors,
                'spent_sec' => LogService::spentSec($startedAt),
            ];
        } catch (AuthenticationException $e) {
            Log::error($method, LogService::context($startedAt, $e));
            $response_state = 401;
            $response = [
                'message' => $e->getMessage(),
                'spent_sec' => LogService::spentSec($startedAt),
            ];
        } catch (ServerException $e) {
            $response = new StandardJsonResponse($e->getResponse());
            $message = $response->message();
            Log::critical($method, compact('response') + LogService::context($startedAt, $e));

            $response_state = $response->statusCode();
            $response = [
                'message' => $message,
                'errors' => $response->errors(),
                'spent_sec' => LogService::spentSec($startedAt),
            ];

        } catch (ClientException $e) {
            $response = new StandardJsonResponse($e->getResponse());

            // ошибки доступа между сервисами не показываем и логируем как критикал.
            if ($response->statusCode() == 403) {
                $message = 'Service temporary unavailable.';
                Log::critical($method . ' '. $message, compact('response') + LogService::context($startedAt, $e));
            } else {
                $message =  $response->message();
                Log::error($method, compact('response') + LogService::context($startedAt, $e));
            }

            $response_state = $response->statusCode();
            $response = [
                'message' => $message,
                'errors' => $response->errors(),
                'spent_sec' => LogService::spentSec($startedAt),
            ];
        } catch (Exception $e) {
            Log::critical($method, LogService::context($startedAt, $e));

            $response_state = 500;
            $response = [
                'message' => 'Internal server error',
                'debug_message' => (config('app.debug') ? $e->getMessage() : null),
                'spent_sec' => LogService::spentSec($startedAt),
            ];
        }

        // Debug
        if (config('app.debug')) {
            if (is_array($response)) {
                $response['sqlQueryLog'] = \DB::getQueryLog();
            } elseif (get_class($response) === 'Illuminate\Http\JsonResponse') {
                $res = $response->getData(true);
                $res['sqlQueryLog'] = \DB::getQueryLog();
                $response->setData($res);
                return $response;
            }
        }

        if (is_array($response)) {
            return response()->json(
                $response,
                $response_state
            );
        } elseif (get_class($response) === 'Illuminate\Http\JsonResponse') {
            return $response;
        }
    }
}
