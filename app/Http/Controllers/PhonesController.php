<?php
namespace App\Http\Controllers;

use App\Context\Common\Log\LogService;
use Illuminate\Support\Facades\Log;
use App\Http\ApiController;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Request as SRequest;

use App\Context\Phones\PhonesService;

class PhonesController extends ApiController
{
    /**
     * Get data by id
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function get (Request $request, int $id): JsonResponse
    {
        $method = __METHOD__;
        $startedAt = microtime(1);

        return $this->tryCatchApiResponse(function () use ($method, $startedAt, $request, $id) {
            // Get data
            $data = app(PhonesService::class)->get($id);
            //
            $data = [
                'result' => 'ok',
                'data' => $data,
                'spent_sec' => LogService::spentSec($startedAt),
            ];
            // Response
            return response()->json($data);
        }, $method);
    }


    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function create (Request $request): JsonResponse
    {
        $method = __METHOD__;
        $startedAt = microtime(1);

        return $this->tryCatchApiResponse(function () use ($method, $startedAt, $request) {
            // Add User id & Create
            $res = app(PhonesService::class)->create($request);
            // Response
            if (isset($res['result']) && $res['result'] === 'ok') {
                $res = [
                    'result' => 'ok',
                    'spent_sec' => LogService::spentSec($startedAt),
                ];
            }
            // Response
            return response()->json($res);
        }, $method);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function update (Request $request, int $id): JsonResponse
    {
        $method = __METHOD__;
        $startedAt = microtime(1);

        return $this->tryCatchApiResponse(function () use ($method, $startedAt, $request, $id) {
            // Stop
            $res = app(PhonesService::class)->update($request, $id);
            // Response
            if (isset($res['result']) && $res['result'] === 'ok') {
                $res = [
                    'result' => 'ok',
                    'spent_sec' => LogService::spentSec($startedAt),
                ];
            }
            // Response
            return response()->json($res);
        }, $method);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function delete (Request $request, int $id): JsonResponse
    {
        $method = __METHOD__;
        $startedAt = microtime(1);

        return $this->tryCatchApiResponse(function () use ($method, $startedAt, $request, $id) {
            // Stop
            $res = app(PhonesService::class)->delete($request, $id);
            // Response
            return response()->json($res);
        }, $method);
    }

}
