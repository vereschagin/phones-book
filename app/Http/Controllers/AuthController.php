<?php
namespace App\Http\Controllers;

use App\Http\ApiController;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class AuthController extends ApiController
{
    public function checkPassw (Request $request): JsonResponse
    {
        $passwHash = $request->get('p', time());
        //
        if ($passwHash === env('ADMIN_PASSWORD_MD5', md5(time().mt_rand(1000,9999)))) {
            return response()->json([
                'result' => 'ok'
            ]);
        }
        //
        return response()->json([
            'result' => 'error'
        ]);
    }
}
