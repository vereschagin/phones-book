<?php

return [

    // влияют на логин в гейтвее
    'users' => [
        'api_url'   => env('SERVICES_USERS_API_URL'),
        'api_token' => env('SERVICES_USERS_API_TOKEN'),

        // влияют на создание SSO кук и автологина на других сайтах
        'sso' => [
            'enabled' => env('SERVICES_USERS_SSO_ENABLED', true),
            'site_id' => env('SERVICES_USERS_SSO_SITE_ID'),
            'secret_key' => env('SERVICES_USERS_SSO_SECRET_KEY'),
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'wmspanel' => [
        'url' => env('SERVICES_WMSPANEL_API_URL'),
        'client_id' => env('SERVICES_WMSPANEL_API_CLIENT_ID'),
        'api_key' => env('SERVICES_WMSPANEL_API_KEY'),
    ],

    'nimble' => [
        'input_rtmp_url' => env('SERVICES_NIMBLE_INPUT_RTMP_URL'),
        'input_app_password' => env('SERVICES_NIMBLE_INPUT_APP_PASSWORD'),
    ],
];
