<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/css/client.css" />
    <title>Phones book</title>
</head>
<body>
    <div id="rmclient-app">
        <div class="p-4"><span class="spinner-border spinner-border-sm me-2" role="status" aria-hidden="true"></span> Loading...</div>
    </div>
    <script>
        var RM = {
            host: window.location.protocol + "//{{ config('app.domain_app') }}/"
        };
    </script>
    <script src="/js/manifest.js"></script>
    <script src="/js/vendor.js"></script>
    <script src="/js/client_app.js"></script>
</body>
</html>
