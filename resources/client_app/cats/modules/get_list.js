let log = rmClientApp.$log.getLogger('cats.get_list')

export default {
    namespaced: true,
    state: {
        ready: true,
        created: false,
        result: null,
        data: null,
    },
    actions: {
        // On created
        created ({ dispatch, state, commit }) {
            if (!state.created) {
                log.debug('[created]')

                dispatch('getData');

                // Были события, обновившие вх. потоки
                rmClientApp.on('cats_up', function () {
                    dispatch('getData');
                })

                // Были события, обновившие вх. потоки
                rmClientApp.on('phones_up', function () {
                    dispatch('getData');
                })

                commit('setCreated')
            }
        },

        // Получение данных
        getData ({ commit }) {
            log.debug('[getData]')
            commit('setResult', 'loading')
            rmClientApp.$axios
                .get('api/cats')
                .then(function (res) {
                    if (res?.data?.result && res?.data?.result === 'ok') {
                        commit('setResult', 'ok')
                        commit('setData', res?.data?.list)
                    } else {
                        commit('setResult', 'error')
                        commit('setData', null)
                    }
                })
                .catch(function(res) {
                    commit('setResult', 'error')
                    commit('setData', null)
                })
        }
    },
    mutations: {
        setCreated (state) {
            state.created = true
        },
        setResult (state, val) {
            state.result = val
        },
        setData (state, val) {
            state.data = val
        },
    },
}
