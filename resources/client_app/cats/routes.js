import CreateCat from './components/CreateForm.vue'
import EditCat from './components/EditForm.vue'
import DeleteCat from './components/DeleteForm.vue'

export const routes = [
    {
        name: 'CreateCat',
        path: '/cats/create/:catId',
        component: CreateCat,
    },
    {
        name: 'EditCat',
        path: '/cats/edit/:catId',
        component: EditCat,
    },
    {
        name: 'DeleteCat',
        path: '/cats/delete/:catId',
        component: DeleteCat,
    },
];
