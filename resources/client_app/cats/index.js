let Vue = window.rmClientVue

import * as VeeValidate from 'vee-validate'
Vue.use(VeeValidate)


// Регистрируем компоненты
import CreateCat from './components/CreateForm.vue'
import EditCat from './components/EditForm.vue'
import DeleteCat from './components/DeleteForm.vue'

Vue.component('CreateCat', CreateCat)
Vue.component('EditCat', EditCat)
Vue.component('DeleteCat', DeleteCat)


// Регистрируем vuex-модули
import CatsGetList from './modules/get_list.js'
import CatsCreate from './modules/create.js'
import CatsUpdate from './modules/update.js'
import CatsDelete from './modules/delete.js'

rmClientApp.$store.registerModule('CatsGetList', CatsGetList)
rmClientApp.$store.registerModule('CatsCreate', CatsCreate)
rmClientApp.$store.registerModule('CatsUpdate', CatsUpdate)
rmClientApp.$store.registerModule('CatsDelete', CatsDelete)


// Регистрируем маршруты
import { routes } from './routes.js'

routes.forEach(route => rmClientApp.$router.addRoute(route))


// Инициализируем root инстанс Vue для рендера корневого компонента AuthApp
new Vue({
    store: rmClientApp.$store,
    router: rmClientApp.$router,
    data: {
        app: rmClientApp,
        instances: [],
    },
    //el: '#rmclient-cats',
    //render: (h) => h(CatsApp), // рендерим корневой компонент бандла Auth внутри el
    created() {
        //
    }
})
