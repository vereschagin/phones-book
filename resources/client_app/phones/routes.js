import CreatePhone from './components/CreateForm.vue'
import EditPhone from './components/EditForm.vue'
import DeletePhone from './components/DeleteForm.vue'

export const routes = [
    {
        name: 'CreatePhone',
        path: '/phones/create/:catId',
        component: CreatePhone,
    },
    {
        name: 'EditPhone',
        path: '/phones/edit/:phoneId',
        component: EditPhone,
    },
    {
        name: 'DeletePhone',
        path: '/phones/delete/:phoneId',
        component: DeletePhone,
    },
];
