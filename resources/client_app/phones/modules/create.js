let log = rmClientApp.$log.getLogger('phones.create')

export default {
    namespaced: true,
    state: {
        ready: false,
        created: false,
        //
        result: null,
        message: null,
        errors: null,
    },
    actions: {
        // On created
        created ({ dispatch, state, commit }) {
            if (!state.created) {
                log.debug('[created]')

                // Если аутефикация уже ясна
                if (rmClientApp?.$store?.state?.AppUser?.isAuthenticated !== null) {
                    commit('setReady');
                }

                // При аутефикации или смене юзера - обновляем список
                rmClientApp.on('user_changed', function () {
                    commit('setReady');
                })

                commit('setCreated')
            }
        },

        submit ({ dispatch, state, commit }, data) {
            // Request
            rmClientApp.$axios
                .post('api/phones', data)
                .then(function (res) {
                    if (res?.data?.result === 'ok') {
                        rmClientApp.emit('phones_up')
                        rmClientApp.toast('success', 'Phone successfully created');
                        rmClientApp.$router.push('/')
                    } else {
                        rmClientApp.toast('error', 'Error!<br>'+res?.data?.message);
                    }
                    commit('setResponse', res?.data || {})
                })
                .catch((error) => {
                    rmClientApp.toast('error', (error.response?.data?.message || 'Error!'));
                    commit('setResponse', error?.response?.data || {})
                })
        },
    },
    mutations: {
        setReady (state) {
            state.ready = true
        },
        setCreated (state) {
            state.created = true
        },
        setResponse (state, data) {
            state.result = data?.result || 'error'
            state.message = data?.message || null
            state.errors = data?.errors || null
        },
    },
}
