let Vue = window.rmClientVue

import * as VeeValidate from 'vee-validate'
Vue.use(VeeValidate)


// Регистрируем компоненты
import CreatePhone from './components/CreateForm.vue'
import EditPhone from './components/EditForm.vue'
import DeletePhone from './components/DeleteForm.vue'

Vue.component('CreatePhone', CreatePhone)
Vue.component('EditPhone', EditPhone)
Vue.component('DeletePhone', DeletePhone)


// Регистрируем vuex-модули
import PhonesGetList from './modules/get_list.js'
import PhonesCreate from './modules/create.js'
import PhonesUpdate from './modules/update.js'
import PhonesDelete from './modules/delete.js'

rmClientApp.$store.registerModule('PhonesGetList', PhonesGetList)
rmClientApp.$store.registerModule('PhonesCreate', PhonesCreate)
rmClientApp.$store.registerModule('PhonesUpdate', PhonesUpdate)
rmClientApp.$store.registerModule('PhonesDelete', PhonesDelete)


// Регистрируем маршруты
import { routes } from './routes.js'

routes.forEach(route => rmClientApp.$router.addRoute(route))


// Инициализируем root инстанс Vue для рендера корневого компонента AuthApp
new Vue({
    store: rmClientApp.$store,
    router: rmClientApp.$router,
    data: {
        app: rmClientApp,
        instances: [],
    },
    //el: '#rmclient-Phones',
    //render: (h) => h(PhonesApp), // рендерим корневой компонент бандла Auth внутри el
    created() {
        //
    }
})
