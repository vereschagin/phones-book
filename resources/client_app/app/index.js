import Vue from 'vue'

global.rmClientVue = Vue

/**
 * Check RM params
 */
if (!RM?.host) {
    throw new Error('Empty RM params')
}

/**
 * Vuex
 */
import Vuex, { mapActions } from 'vuex'

Vue.use(Vuex)

/**
 * Store
 */
import VuexStore from './store.js'
import createPersistedState from 'vuex-persistedstate'

const store = new Vuex.Store(VuexStore)
const persistedStore = createPersistedState({
    paths: [
    ],
})

/**
 * Toasts
 * Всплывающие уведомления
 * https://vue-toastification.maronato.dev/
 */
import Toast from "vue-toastification";

Vue.use(Toast, {
    transition: "Vue-Toastification__bounce",
    maxToasts: 20,
    newestOnTop: true
});


/**
 * Router
 */
import Router from 'vue-router'
import Error404 from './components/Error404.vue'

Vue.use(Router)
const router = new Router({
    // по дефолту все маршруты добавляются с бандлами, в том числе корневой
    // в конце подключаются appWildcardRoutes
    mode: 'history',
})

// глобальные wildcards (подключаются после всех)
let appWildcardRoutes = [
    {
        path: '/',
        component: App,
        name: 'index',
    },
    {
        path: '/:catchAll(.*)',
        component: Error404,
        name: '404'
    },
]

/**
 * Event bus
 */
import EventBus from './event-bus.js'

Vue.prototype.$eventBus = EventBus

/**
 * Интеграция логирования
 */
import logger from 'loglevel'

Vue.prototype.$log = logger

/**
 * Axios
 */
import axios from 'axios'

axios.defaults.baseURL = RM.host.replace(/\/$/, '') + '/'
axios.defaults.withCredentials = true
axios.defaults.xsrfCookieName = 'XSRF-TOKEN'
axios.defaults.xsrfHeaderName = 'X-XSRF-TOKEN'
axios.defaults.timeout = 30000
Vue.prototype.$axios = axios


/*
 * App
 */
import App from './components/App.vue'

let log = logger.getLogger('rmClientApp')
window.rmClientApp = new Vue({
    store: store,
    router: router,
    mixins: [
    ],
    data: {
        base_url: null,
        instances: [],
        modulesReady: {},
        readyCallbacks: [],
    },
    methods: {
        // Events
        emit: function (event, payload) {
            log.warn('[$emit] event "' + event + '" with parameters:', payload)
            this.$emit(event, payload)
        },
        on: function (event, func) {
            log.warn('[$on] event "' + event + '" call function:', func)
            this.$on(event, func)
        },
        once: function (event, func) {
            log.warn('[$once] event "' + event + '" call function:', func)
            this.$once(event, func)
        },
        off: function (event, func) {
            log.warn('[$off] event "' + event + '" call function:', func)
            this.$off(event, func)
        },

        camelCase: function (string) {
            return string.split('_').map((chunk, i) => {
                return (i === 0)
                    ? chunk.toLowerCase()
                    : chunk.charAt(0).toUpperCase() + chunk.slice(1).toLowerCase()
            }).join('')
        },

        // Format date
        formatDate: function (value) {
            if (value) {
                return this.$luxon(
                    String(value),
                    {
                        output: {
                            zone: "client",
                            format: "dd.MM.yyyy HH:mm",
                        }
                    }
                );
            }
            return'-';
        },

        toast (type, content) {
            this.$toast(content, {
                type: type, // "success", "error", "default", "info" and "warning"
                position: "bottom-right",
                timeout: 6000,
                closeOnClick: true,
                pauseOnFocusLoss: true,
                pauseOnHover: true,
                draggable: true,
                draggablePercent: 0.3,
                showCloseButtonOnHover: false,
                hideProgressBar: false,
                closeButton: "button",
                icon: true,
                rtl: false,
            });
        },


        // Проверка все ли vuex модули получили статус Ready (каждый модуль сам решает в какой момент установить такой статус)
        checkModulesReady: function () {
            this.modulesReady = {}
            let allReady = true
            let keys = Object.keys(this.$store.state)
            for (var i = 0; i < keys.length; i++) {
                let k = keys[i]
                // по дефолту vuex модули являются готовыми (чтобы не ломалась логика при добавлении новых модулей, у которых нет ready аттрибута)
                let isReady = true
                // смотрим соотв свойство модуля (его может не быть, если нету то используется дефолтное значение)
                if (this.$store.state[k].hasOwnProperty('ready')) {
                    isReady = Boolean(this.$store.state[k].ready)
                }
                this.modulesReady[k] = isReady
                if (!isReady) {
                    allReady = false
                    break
                }
            }
            if (allReady) {
                log.warn('rmClientApp_all_modules_ready')
                this.emit('rmClientApp_all_modules_ready')
            } else {
                setTimeout(function () {
                    rmClientApp.checkModulesReady()
                }, 500)
            }
        },

        // Render all components within '.rmclient-render' containers
        rmClientRender: function () {
            log.warn('[rmClientRender]')
            let tpls = document.querySelectorAll('.rmclient-render')
            for (var i = 0; i < tpls.length; i++) {
                let vm = new Vue({
                    store: this.$store,
                    router: this.$router,
                    el: tpls[i]
                })
                this.instances.push(vm)
            }
            this.checkModulesReady()
        },
    },
    created () {
        this.base_url = RM.host.replace(/\/$/, '') + '/'

        // Добавляем подключение wildcard маршрутов в очередь, чтобы добавились
        // после того как все скрипты и маршруты из бандлов инициализированы
        this.readyCallbacks.push(() => {
            appWildcardRoutes.forEach(route => router.addRoute(route))
        })
    },
    mounted: function () {

        // event
        this.emit('rmClientApp_mounted')

        // если все ресурсы страницы в этом момент загружены
        if (document.readyState === 'complete') {
            this.rmClientRender()
        }

    },
    render: h => h(App)
})

/**
 * При изменении состояния документа, проверяем не загружен ли он уже
 * https://developer.mozilla.org/en-US/docs/Web/API/Document/readyState
 */
document.onreadystatechange = () => {
    log.warn('[document.onreadystatechange()] document.readyState =', document.readyState, Boolean(rmClientApp))

    //  Если readyState=complete - cтраница и все дополнительные ресурсы уже загружены, запускаем доп. инициализации
    if (document.readyState === 'complete' && rmClientApp) {
        rmClientApp.rmClientRender()
        // вызываем все другие отложенные действия которые есть
        rmClientApp.readyCallbacks.forEach(cb => cb())

        // инициализируем persisted store после того как динамические модули подключены
        // https://github.com/robinvdvleuten/vuex-persistedstate/pull/225
        persistedStore(store)

    }
}

// Функция для динамического монтирования и рендера дополнительных Vue-инстансов
window.mountVueInstance = function (vm, elementId) {
    log.debug('[app] mountVueInstance:', vm, elementId)
    return new Promise((resolve) => {
        function mount () {
            let el = document.getElementById(elementId)
            if (el === null) {
                el = document.createElement('div')
                el.id = elementId
                document.body.appendChild(el)
            }
            vm.$mount('#' + elementId)
            log.debug('[app.mountVueInstance] Vue instance mounted to #' + elementId, vm)

            resolve(el)
        }

        if (document.readyState === 'interactive' || document.readyState === 'complete') {
            // если DOM прогружен, монтируем сразу
            mount()
        } else {
            // если не прогружен, ждем и монтируем
            document.addEventListener('DOMContentLoaded', mount)
        }
    })
}

// Mount App.vue component (auto create div #rmclient-app)
mountVueInstance(rmClientApp, 'rmclient-app')

// DateTime converter
Vue.filter(
    'formatDate',
    function(value){
        return rmClientApp.formatDate(value)
    }
);

