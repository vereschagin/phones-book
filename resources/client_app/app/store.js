import createLogger from 'vuex/dist/logger'

const debug = process.env.NODE_ENV !== 'production'

// Modules

let plugins = [
]


if (debug) {
    // plugins.push(createLogger())
}

export default {
    modules: {
    },
    strict: debug,
    plugins: plugins
}
