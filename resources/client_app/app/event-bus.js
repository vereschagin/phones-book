import Vue from 'vue'

const EventBus = new Vue({
    methods: {
        emit: function (event, payload) {
            this.$emit(event, payload);
        },

        on: function (event, func) {
            this.$on(event, func);
        },

        off: function (event, func) {
            this.$off(event, func);
        },
    },
});

export default EventBus;
