const mix = require('laravel-mix');
const webpack = require('webpack')


// Paths
const clientDir = './resources/client_app/'
const distDir = 'public'

// Bundles by context's
mix
    .setResourceRoot('../')
    .setPublicPath(distDir)

    // Client App CSS
    .combine([
        // Bootstrap
        './node_modules/bootstrap/dist/css/bootstrap.min.css',
        // Vue - Toasts
        './node_modules/vue-toastification/dist/index.css',
        // Fonts
        './node_modules/font-awesome/css/font-awesome.css',

    ],'public/css/client.css')
    // Fonts
    .copy(
        './node_modules/font-awesome/fonts',
        distDir + '/fonts'
    )
    // Client App JS
    .js([
        './node_modules/bootstrap/dist/js/bootstrap.bundle.js',
        clientDir + 'app/index.js',
        clientDir + 'cats/index.js',
        clientDir + 'phones/index.js',
    ], 'js/client_app.js')

    .dump()
    .version()
    .extract([
        'vue',
        'vuex',
        'axios',
        'loglevel',

        //
        'vue-router',
        'vuex-router-sync',
        'vue-js-modal',
        'vee-validate',
        'vue-cookies',
        'process',
    ])

// Additional webpack config settings
mix.webpackConfig({
    plugins: [
        new webpack.EnvironmentPlugin({
            NODE_ENV: 'development', // use 'development' unless process.env.NODE_ENV is defined
            DEBUG: false
        }),
    ],
    resolveLoader: {
        alias: {
            'tpl': 'lodash-template-webpack-loader'
        }
    },
    resolve: {
        alias: {
            'vue$': 'vue/dist/vue.esm.js'
        }
    },
    stats: {
        children: true
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                type: 'asset/source',
                use: [
                    'style-loader',
                    {
                        loader: 'css-loader',
                        options: {
                            importLoaders: 1
                        }
                    },
                ],
            },
            {
                test: /\.(woff|woff2|eot|ttf|svg)(\?.*$|$)/,
                use: 'file-loader'
            },
            {
                test: '/(\\.(png|jpe?g|gif|webp)$|^((?font).)*\\.svg$)/',
                use: [
                    {
                        'loader': 'file-loader',
                        'options': {
                            'publicPath': '/assets/',
                            'esModule': false
                        }
                    }
                ]
            },
        ]
    }
}).vue({
    version: 2,
    jsx: true,
})
